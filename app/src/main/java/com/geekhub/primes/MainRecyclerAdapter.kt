package com.geekhub.primes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_main.view.*

class MainRecyclerAdapter : RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder>() {
    public val numbers = mutableListOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_main, parent, false)
        return ViewHolder(view)
    }


    fun addNumber(num: Int) {
        numbers.add(num)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return numbers.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(numbers[position])
    }


    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun onBind(int: Int) {
            if (int == 822) {
                view.number.text = "Generate stopped"
                numbers.remove(822)
            } else {
                view.number.text = int.toString()
            }
        }
    }
}