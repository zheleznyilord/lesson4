package com.geekhub.primes.calculator

import android.content.Context
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import java.util.concurrent.TimeUnit


class Calculator(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {

    override fun doWork(): Result {
        var pos = inputData.getInt("lastnumber", 0)
        var generate: Int
        while (!this.isStopped) {
            if (pos == 0) {
                generate = 1
            } else {
                generate = generate(pos)
            }

            var applicationContext = applicationContext
            setProgressAsync(Data.Builder().putInt("progress", generate).build())

            TimeUnit.SECONDS.sleep(5)
            pos = generate
        }
//        val output = Data.Builder()
//            .putInt("generate", generate)
//            .build()
        return Result.success()
    }

    private fun generate(last: Int): Int {
        if (last == 1) {
            return 2
        }
        if (last == 2) {
            return 3
        }
        val isPrime: Boolean = true
        var newNum = last + 1
        while (isPrime) {
            for (i in 2..newNum / 2) {
                if (checkDel(newNum, i)) {
                    newNum++
                    break
                }
                if (i == newNum / 2) {
                    return newNum
                }
            }
        }
        return 0
    }

    private fun checkDel(value: Int, del: Int): Boolean {
        return value % del == 0
    }

}