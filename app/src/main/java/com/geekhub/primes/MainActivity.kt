package com.geekhub.primes

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.work.Data
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.geekhub.primes.calculator.Calculator
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val adapter by lazy { MainRecyclerAdapter() }
    private lateinit var id:UUID

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.start -> {
                val inputData = Data.Builder()
                    .putInt("lastnumber",
                        if (adapter.numbers.isEmpty()){0}else{adapter.numbers[adapter.numbers.size-1]})
                    .build()

                val myWorkRequest = OneTimeWorkRequest.Builder(Calculator::class.java)
                    .setInputData(inputData).build()

                WorkManager.getInstance().enqueue(myWorkRequest)
                id = myWorkRequest.id

                WorkManager.getInstance().getWorkInfoByIdLiveData(id).observe(this, Observer {
                       // workInfo ->
//                    when (workInfo.state) {
//                        WorkInfo.State.SUCCEEDED -> {
//                            val array = workInfo.outputData.getInt("generate",0)
//                            array.let { adapter.addNumber(it) }
//                        }
//                        WorkInfo.State.CANCELLED -> {adapter.addNumber(822)}
//                        WorkInfo.State.FAILED -> {adapter.addNumber(824)}
//                        WorkInfo.State.RUNNING -> {}
//                        else -> { }
//                    }
                    it?.progress?.getInt("progress", 0)?.apply {
                        if (this != 0 && !adapter.numbers.contains(this)) {
                            adapter.addNumber(this)
                        }
                    }
                })
            }
            R.id.stop -> {
                WorkManager.getInstance().cancelWorkById(id)
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        start.setOnClickListener(this)
        stop.setOnClickListener(this)

        recycler_view.adapter = adapter
    }

    override fun onSaveInstanceState(outState: Bundle) {
        val bundle = Bundle()
        bundle.putInt("count", adapter.itemCount)
        val items = adapter.numbers
        for (item in items) {
            bundle.putInt(items.indexOf(item).toString(), item)
        }
        outState.putBundle("numbers", bundle)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        val bundle = savedInstanceState.getBundle("numbers")
        val count = bundle?.getInt("count")?.minus(1)
        adapter.numbers.clear()
        for (i in 0..count!!) {
            val item = bundle.getInt(i.toString())
            adapter.numbers.add(item)
        }

        super.onRestoreInstanceState(savedInstanceState)
    }


}
